package com.example.diegojoza.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadParcial extends AppCompatActivity {
    Button botonlogin, botonbuscar, botonregistrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_parcial);

        botonlogin = findViewById(R.id.btnlogin);
        botonbuscar =  findViewById(R.id.btnbuscar);
        botonregistrar =  findViewById(R.id.btnregistrar);

        botonlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, ActividadLogin.class);
                startActivity(intent);
            }
        });


        botonregistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, ActividadRegistrar.class);
                startActivity(intent);
            }
        });

        botonbuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActividadParcial.this, ActividadBuscar.class);
                startActivity(intent);
            }
        });

    }
}
